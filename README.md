# 仮想通貨取引ボット作り（もくもく会）

2017.1.8

仮想通貨取引のBotを作りながら、ブロックチェーンや機械学習といったこれから楽しみな技術の理解を深めていきましょ！

## 環境構築

### Chrome Remote Desktop

ペアプロ・モブプロ用

- [Chrome Remote Desktop - Chrome Web Store](https://chrome.google.com/webstore/detail/chrome-remote-desktop/gbchcmhmhahfdphkhkmpfmihenigjmpp?hl=en)
- [Free Screen Sharing and Online Meeting Software | Screenleap](https://www.screenleap.com/)

### Python

Python3 環境
requirements.txt

### Python api clients
- [s4w3d0ff/python-poloniex: Poloniex API wrapper for Python 2.7 & 3](https://github.com/s4w3d0ff/python-poloniex)
- [yagays/pybitflyer: Python wrapper for bitFlyer's REST API.](https://github.com/yagays/pybitflyer)
- [kmn/coincheck: Coincheck API Library for Python](https://github.com/kmn/coincheck)
- [E-Tsubo/zaifapi: zaifのAPIを簡単にコール出来るようにしました。](https://github.com/E-Tsubo/zaifapi)

### Api key
- [coincheck（コインチェック）](https://coincheck.com/ja/api_settings)
- [bitFlyer Lightning ビットコイン取引所](https://lightning.bitflyer.jp/developer)
- [Cryptocurrency exchange - Zaif](https://zaif.jp/api_keys)

### Deploy

GCEが制限つき無料のようなので

Free GCE VM instance

	1 f1-micro instance per month (US regions only, excluding Northern Virginia)
	30 GB-months HDD, 5 GB-months snapshot
	1 GB network egress from North America to all region destinations per month (excluding China and Australia)

1. [Google Cloud Platform](https://cloud.google.com/)
	- Google Cloud Platform の利用登録
	- プロジェクトの作成
2. [Google Compute Engine](https://console.cloud.google.com/compute/instances)
	- インスタンス作成
3. [Installing Google Cloud SDK](Installing Cloud SDK  |  Cloud SDK Documentation  |  Google Cloud Platform
https://cloud.google.com/sdk/downloads)
4. ssh

```bash
REGION=us-central1-a
gcloud config set compute/zone ${REGION}
gcloud compute instances list
INSTANCE=vcbot
gcloud compute ssh ${INSTANCE}
```
## Chart class (jupiter note)

```bash
jupyter notebook
```

chart.ipynb

## Trader class (based on bitFlyer api)

trader.py

- 成行買い注文
- 指値買い注文
- 成行売り注文
- 指値売り注文

### Usage

bash

```bash
API=xxxxxxxxxxxxxxxxxxxx
SECRET=xxxxxxxxxxxxxxxxxxxx
```

python

```Python
bitflyer = pybitflyer.API(api_key=os.environ["API"], api_secret=os.environ["SECRET"])
Trader(bitflyer, ammount).buy_btc_mkt
```

## Market class
	分析手法

# 参考文献
- [ビットコインの仕組み - ビットコイン](https://bitcoin.org/ja/how-it-works)
- [日本語で読むビットコイン原論文 [by Satoshi Nakamoto] | Coincheck（コインチェック）](https://coincheck.com/blog/292)
- [仮想通貨の完全自動売買をやってみよう　その１ | Money Forward Engineers' Blog](https://moneyforward.com/engineers_blog/2017/05/23/virtual-currency-automatic-trading1/)
- [Bitcoin Wiki](https://en.bitcoin.it/wiki/Main_Page)
- [仮想通貨自動取引入門 - Qiita](https://qiita.com/shionhonda/items/bd2a7aaf143eff4972c4#%E3%83%93%E3%83%83%E3%83%88%E3%82%B3%E3%82%A4%E3%83%B3%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6)
- [Bitcoinを技術的に理解する](https://www.slideshare.net/kenjiurushima/20140602-bitcoin1-201406031222)
- [仮想通貨取引所のPoloniexからAPI経由でデータ取得し、ディープラーニング（Chainer）で翌日の価格予測をしよう - Qiita](https://qiita.com/yoshizaki_kkgk/items/79f4056901dd9c059afb)
