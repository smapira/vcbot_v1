class Trader:
    def __init__(self, _api, _amount):
        self.api = _api
        self.amount = _amount

    def buy_btc_mkt(self):
        self.amount = int(self.amount * 100000000) / 100000000
        buy = self.api.sendchildorder(product_code="BTC_JPY", child_order_type="MARKET", side="BUY", size=self.amount,
                                      minute_to_expire=10, time_in_force="GTC")
        print("BUY ", self.amount, "BTC")
        print(buy)

    def buy_btc_lmt(self, _price):
        self.amount = int(self.amount * 100000000) / 100000000
        buy = self.api.sendchildorder(product_code="BTC_JPY", child_order_type="LIMIT", price=_price, side="BUY",
                                      size=self.amount, minute_to_expire=10, time_in_force="GTC")
        print("BUY ", self.amount, "BTC")
        print(buy)

    def sell_btc_mkt(self):
        self.amount = int(self.amount * 100000000) / 100000000
        sell = self.api.sendchildorder(product_code="BTC_JPY", child_order_type="MARKET", side="SELL", size=self.amount,
                                       minute_to_expire=10, time_in_force="GTC")
        print("SELL ", self.amount, "BTC")
        print(sell)

    def sell_btc_lmt(self, _price):
        self.amount = int(self.amount * 100000000) / 100000000
        sell = self.api.sendchildorder(product_code="BTC_JPY", child_order_type="LIMIT", price=_price, side="SELL",
                                       size=self.amount, minute_to_expire=10, time_in_force="GTC")
        print("SELL ", self.amount, "BTC")
        print(sell)
