import poloniex
import time
import pandas as pd
import matplotlib.pyplot as plt

api = poloniex.Poloniex()
chart_data = api.returnChartData('USDT_BTC', period=300, start=time.time() - api.DAY * 10,
                                 end=time.time())
df = pd.DataFrame(chart_data)
df.head(10)
data_s = pd.Series.rolling(df['close'], 12 * 24).mean()
data_l = pd.Series.rolling(df['close'], 12 * 24 * 4).mean()

plt.style.use('dark_background')
plt.plot(data_s, color='#f1c40f')
plt.plot(data_l, color='#2980b9')
plt.show()
