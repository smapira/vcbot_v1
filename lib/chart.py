import pybitflyer
import numpy as np
import matplotlib.pyplot as plt
from IPython.display import display, clear_output
import time
%matplotlib inline

fig = plt.figure(figsize=(16, 10))
axe = fig.add_subplot(111)
start_time = time.time()
bitflyer = pybitflyer.API()
rows = []

while True:
    tick = bitflyer.ticker()
    clear_output(wait=True)
    rows = np.append(rows, tick['ltp'])
    axe.plot(rows, "black", linewidth=2, label="Row price")
    axe.set_title("Row price")
    display(fig)
    axe.cla()
    time.sleep(60.0 - ((time.time() - start_time) % 60.0))
